package cn.beckbi.dao.mapper1;

import cn.beckbi.model.User;

/**
 * @program: mult-db
 * @description: user1 mapper
 * @author: bikang
 * @create: 2022-04-23 20:10
 */
public interface User1Mapper {
    User getUserById(long id);
}
