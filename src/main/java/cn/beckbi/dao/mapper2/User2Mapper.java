package cn.beckbi.dao.mapper2;

import cn.beckbi.model.User;

/**
 * @program: mult-db
 * @description: user2 mapper
 * @author: bikang
 * @create: 2022-04-23 20:11
 */
public interface User2Mapper {
    User getUserById(long id);
}
