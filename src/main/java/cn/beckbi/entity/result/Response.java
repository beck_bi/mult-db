package cn.beckbi.entity.result;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import javafx.util.Pair;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

/**
 * @program: mult-db
 * @description:
 * @author: bikang
 * @create: 2022-04-23 19:54
 */
@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Response<T> {
    private Integer code;
    private T result;
    private String message;
    private String traceId;

    private String request;


}
