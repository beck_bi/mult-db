package cn.beckbi.service.impl;

import cn.beckbi.dao.mapper1.User1Mapper;
import cn.beckbi.dao.mapper2.User2Mapper;
import cn.beckbi.model.User;
import cn.beckbi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @program: mult-db
 * @description: impl
 * @author: bikang
 * @create: 2022-04-23 20:18
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    User1Mapper user1Mapper;

    @Autowired
    User2Mapper user2Mapper;

    @Override
    public User getByIdFromDb1(Long id) {
        return user1Mapper.getUserById(id);
    }

    @Override
    public User getByIdFromDb2(Long id) {
        return user2Mapper.getUserById(id);
    }
}
