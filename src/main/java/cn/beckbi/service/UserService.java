package cn.beckbi.service;

import cn.beckbi.model.User;

/**
 * @program: mult-db
 * @description: user service
 * @author: bikang
 * @create: 2022-04-23 20:17
 */
public interface UserService {

    User getByIdFromDb1(Long id);

    User getByIdFromDb2(Long id);
}
