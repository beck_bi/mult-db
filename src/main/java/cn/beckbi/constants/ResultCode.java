package cn.beckbi.constants;

/**
 * @program: mult-db
 * @description:
 * @author: bikang
 * @create: 2022-04-23 19:56
 */

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum  ResultCode {
    /**
     * result
     */
    SUCCESS("成功", 0);
    private String desc;
    private Integer code;

}
