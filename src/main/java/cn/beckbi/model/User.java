package cn.beckbi.model;

import lombok.Data;

/**
 * @program: mult-db
 * @description: user
 * @author: bikang
 * @create: 2022-04-23 19:49
 */
@Data
public class User {

    private Long id;
    private String name;
    private Integer age;
    private String email;
}
