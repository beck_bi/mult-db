package cn.beckbi.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;

/**
 * @program: mult-db
 * @description:
 * @author: bikang
 * @create: 2022-04-23 20:03
 */
@Configuration
public class SpringDataSourceConfig {

    @Bean("DB1")
    @ConfigurationProperties("spring.datasource.db1")
    DataSource DB1() {
        return DataSourceBuilder.create().build();
    }


    @Bean("DB2")
    @ConfigurationProperties("spring.datasource.db2")
    DataSource DB2() {
        return DataSourceBuilder.create().build();
    }
}
