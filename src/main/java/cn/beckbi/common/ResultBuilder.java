package cn.beckbi.common;

import cn.beckbi.constants.ResultCode;
import cn.beckbi.entity.result.Response;
import com.alibaba.fastjson.JSON;
import lombok.experimental.UtilityClass;

/**
 * @program: mult-db
 * @description: ResultBuilder
 * @author: bikang
 * @create: 2022-04-23 19:55
 */
@UtilityClass
public class ResultBuilder {

    public static <T> String builder(T data) {
        Response<T> r = Response.<T>builder()
                .code(ResultCode.SUCCESS.getCode())
                .result(data)
                .message("成功")
                .build();
        return JSON.toJSONString(r);
    }

    public static <T> Response<T> builderResp(T data) {
        return Response.<T>builder()
                .code(ResultCode.SUCCESS.getCode())
                .result(data)
                .message(ResultCode.SUCCESS.name())
                .build();
    }


    public static <T> Response<T> builderFailed(ResultCode resultCode) {
        return Response.<T>builder()
                .code(resultCode.getCode())
                .message(resultCode.name())
                .build();
    }

}
