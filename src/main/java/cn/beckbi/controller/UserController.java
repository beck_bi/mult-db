package cn.beckbi.controller;

import cn.beckbi.common.ResultBuilder;
import cn.beckbi.entity.result.Response;
import cn.beckbi.model.User;
import cn.beckbi.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: mult-db
 * @description: user
 * @author: bikang
 * @create: 2022-04-23 19:51
 */

@Slf4j
@RequestMapping(value="api/v1/user",produces = "application/json;charset=utf-8")
@RestController
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping(value = "/db1/{id}")
    public Response<User> getDB1User1(@PathVariable("id") Long id) {

        return ResultBuilder.builderResp(userService.getByIdFromDb1(id));
    }

    @GetMapping(value = "/db2/{id}")
    public Response<User> getDB2User2(@PathVariable("id") Long id) {
        return ResultBuilder.builderResp(userService.getByIdFromDb2(id));
    }





}
